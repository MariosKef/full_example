import numpy as np

def sqrt_check(arg):
	"""
	Check if sqrt is 
	defined. If it is
	it returns it
	otherwise it returns
	None

	arg: input argument
	return: if defined 
	returns the sqrt of
	the input
	"""

	temp = None
	if arg >= 0:
		print('sqrt is well defined')
		temp = np.sqrt(arg)
	else:
		print(f'{arg}<0. sqrt is not defined')

	return temp


def test_func():
	assert sqrt_check(4)==2
